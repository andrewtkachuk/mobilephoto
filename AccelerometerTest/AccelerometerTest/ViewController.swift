//
//  ViewController.swift
//  AccelerometerTest
//
//  Created by Andrew Tkachuk on 4/20/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit
import CoreMotion

private let cellIdentifier = "accelerationsCell"

class ViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    let motionManager = CMMotionManager()
    var accelerometerData: [CMAccelerometerData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        motionManager.startAccelerometerUpdates(to: OperationQueue.main) { [weak self] (data, error) in
            if let trueData = data {
                self?.accelerometerData.append(trueData)
                self?.tableView.reloadData()
            }
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accelerometerData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? AccelerationTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configureWithAccelerometerData(accelerometerData[indexPath.row])
        return cell
    }
}
