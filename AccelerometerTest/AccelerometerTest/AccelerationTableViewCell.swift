//
//  AccelerationTableViewCell.swift
//  AccelerometerTest
//
//  Created by Andrew Tkachuk on 4/20/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit
import CoreMotion

class AccelerationTableViewCell: UITableViewCell {
    @IBOutlet private weak var xAcceleration: UILabel!
    @IBOutlet private weak var yAcceleration: UILabel!
    @IBOutlet private weak var zAcceleration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureWithAccelerometerData(_ data: CMAccelerometerData) {
        xAcceleration.text = data.acceleration.x.description
        yAcceleration.text = data.acceleration.y.description
        zAcceleration.text = data.acceleration.z.description
    }

}
