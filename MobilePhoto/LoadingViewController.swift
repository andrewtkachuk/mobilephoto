//
//  LoadingViewController.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 3/3/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit

private let segueIdentifier = "photos"

class LoadingViewController: UIViewController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        PhotosManager.shared.loadPhotos { [weak self] success in
            self?.performSegue(withIdentifier: segueIdentifier, sender: self)
        }
    }

}
