//
//  DataService.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 3/3/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import Foundation
import Alamofire

private enum Constants {
    static let basicURL = "https://cloudserviceapp.herokuapp.com/"
    static let headers: HTTPHeaders = ["Accept": "application/json"]
    static let uploadHeaders: HTTPHeaders = ["Content-type": "multipart/form-data",
                                             "Accept": "application/json"]
    static let titleKey = "title"
    static let imageKey = "image"
    static let mimeType = "image/jpeg"
}

class DataService {
    static func loadData(fullPath: String, completionBlock: @escaping (Data?) -> Void) {
        request(fullPath).responseData { data in
            completionBlock(data.result.value)
        }
    }
    
    static func loadJSON(path: String, completionBlock: @escaping (Any?) -> Void) {
        request(Constants.basicURL + path, headers: Constants.headers).responseJSON { data in
            completionBlock(data.result.value)
        }
    }
    
    static func deletePhoto(path: String, completionBlock: @escaping (DefaultDataResponse) -> Void) {
        request(Constants.basicURL + path, method: .delete).response { response in
            completionBlock(response)
        }
    }
    
    static func addPhoto(path: String,
                         title: Data,
                         photoData: Data,
                         completionBlock: @escaping (Bool) -> Void) {
        DataService.sendPhoto(path: path,
                              title: title,
                              photoData: photoData,
                              method: .post,
                              completionBlock: completionBlock)
    }
    
    static func editPhoto(path: String,
                          title: Data,
                          photoData: Data,
                          completionBlock: @escaping (Bool) -> Void) {
        DataService.sendPhoto(path: path,
                              title: title,
                              photoData: photoData,
                              method: .patch,
                              completionBlock: completionBlock)
    }
    
    private static func sendPhoto(path: String,
                          title: Data,
                          photoData: Data,
                          method: HTTPMethod,
                          completionBlock: @escaping (Bool) -> Void) {
        upload(multipartFormData: { (multipartData) in
            multipartData.append(title, withName: Constants.titleKey)
            multipartData.append(photoData,
                                 withName: Constants.imageKey,
                                 fileName: Constants.imageKey,
                                 mimeType: Constants.mimeType)
        }, usingThreshold: UInt64(),
           to: Constants.basicURL + path,
           method: method,
           headers: Constants.uploadHeaders) { (result) in
            switch result {
            case .success(request: let request, streamingFromDisk: _, streamFileURL: _):
                request.responseJSON(completionHandler: { (response) in
                    if response.error == nil {
                        completionBlock(true)
                    } else {
                        completionBlock(false)
                    }
                })
            case .failure(_):
                completionBlock(false)
            }
        }
    }
}
