//
//  PhotosViewController.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 3/3/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit

private enum Constants {
    static let cellIdentifier = "photo"
    static let rowHeight: CGFloat = 310
    static let warningTitle = "Warning"
    static let warningMessage = "Are you sure you want to delete this photo?"
    static let deleteTitle = "Delete"
    static let cancelTitle = "Cancel"
    static let addSegueIdentifier = "addPhoto"
    static let editSegueIdentifier = "editPhoto"
}

class PhotosViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var waitingView: UIView!
    
    private var temporaryPhoto: Photo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = Constants.rowHeight
        tableView.tableFooterView = UIView()
        waitingView.isHidden = true
    }
    
    @IBAction func addNewPhoto(_ sender: UIButton) {
        performSegue(withIdentifier: Constants.addSegueIdentifier, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? AddPhotoViewController {
            controller.delegate = self
            if segue.identifier == Constants.editSegueIdentifier {
                controller.configureForEditing(with: temporaryPhoto)
            }
        }
    }
}

extension PhotosViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PhotosManager.shared.photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier) as? PhotoCell else {
            return UITableViewCell()
        }
        cell.configure(with: PhotosManager.shared.photos[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension PhotosViewController: PhotoCellDelegate {
    func didPressDeletePhoto(id: String) {
        let alert = UIAlertController(title: Constants.warningTitle,
                                      message: Constants.warningMessage,
                                      preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: Constants.deleteTitle, style: .destructive) { [weak self] _ in
            self?.waitingView.isHidden = false
            PhotosManager.shared.deletePhoto(id: id, completionBlock: { [weak self] success in
                if success {
                    self?.tableView.reloadData()
                }
                self?.waitingView.isHidden = true
            })
        }
        alert.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: Constants.cancelTitle,
                                         style: .cancel,
                                         handler: nil)
        alert.addAction(cancelAction)
        
        present(alert,
                animated: true,
                completion: nil)
    }
    
    func didPressEditPhoto(photo: Photo) {
        temporaryPhoto = photo
        performSegue(withIdentifier: Constants.editSegueIdentifier, sender: self)
    }
}

extension PhotosViewController: AddPhotoControllerProtocol {
    func didPressCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    func didSavePhoto() {
        dismiss(animated: true, completion: nil)
        waitingView.isHidden = false
        PhotosManager.shared.loadPhotos { [weak self] success in
            if success {
                self?.tableView.reloadData()
            }
            self?.waitingView.isHidden = true
        }
    }
}
