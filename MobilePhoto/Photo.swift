//
//  Photo.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 3/3/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import Foundation

private enum Constants {
    static let titleKey = "title"
    static let urlKey = "url"
    static let idKey = "id"
    static let innerIdKey = "$oid"
}

class Photo {
    let id: String
    let title: String
    let url: String
    
    init(from json: [String: Any]) {
        guard let name = json[Constants.titleKey] as? String,
            let link = json[Constants.urlKey] as? String,
            let idDictionary = json[Constants.idKey] as? [String: String],
            let identifier = idDictionary[Constants.innerIdKey] else {
                title = ""
                url = ""
                id = ""
            return
        }
        title = name
        url = link
        id = identifier
    }
}
