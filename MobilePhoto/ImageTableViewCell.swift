//
//  ImageTableViewCell.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 4/14/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit

protocol ImageTableViewCellDelegate: class {
    func shouldPresentImagePicker(with source: UIView)
}

class ImageTableViewCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    
    weak var delegate: ImageTableViewCellDelegate?
    var photoImage: UIImage? {
        didSet {
            photoImageView.image = photoImage
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(selectImageTapped))
        photoImageView.addGestureRecognizer(recognizer)
    }

    @objc
    func selectImageTapped() {
        delegate?.shouldPresentImagePicker(with: photoImageView)
    }
}
