//
//  PhotosManager.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 3/2/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import Foundation
import UIKit
import CoreData

private enum Constants {
    static let placeholderImage = "empty"
    static let entityName = "PhotoModel"
    static let idKey = "id"
    static let cacheKey = "imageCache"
}

class PhotosManager {
    static let shared = PhotosManager()
    var photos: [Photo]
    
    private init() {
        photos = []
    }
    
    func loadPhotos(completionBlock: @escaping (Bool) -> Void) {
        DataService.loadJSON(path: "") { [weak self] data in
            guard let response = data as? [[String: Any]] else {
                completionBlock(false)
                return
            }
            self?.photos.removeAll()
            for item in response {
                self?.photos.append(Photo(from: item))
            }
            completionBlock(true)
        }
    }
    
    func loadPhoto(_ photo: Photo, completionBlock: @escaping (UIImage) -> Void) {
        DataService.loadData(fullPath: photo.url) { [weak self] response in
            guard let data = response, let image = UIImage(data: data) else {
                completionBlock(UIImage(named: Constants.placeholderImage) ?? UIImage())
                return
            }
            self?.cache(image: image, for: photo)
            completionBlock(image)
        }
    }
    
    func uploadPhoto(title: String, photoImage: UIImage, completionBlock: @escaping (Bool) -> Void) {
        guard let titleData = title.data(using: .utf8),
            let photoData = photoImage.jpegData(compressionQuality: 1.0) else {
            completionBlock(false)
                return
        }
        DataService.addPhoto(path: "images",
                              title: titleData,
                              photoData: photoData) { (success) in
                                completionBlock(success)
        }
    }
    
    func editPhoto(id: String,
                   title: String,
                   photoImage: UIImage,
                   completionBlock: @escaping (Bool) -> Void) {
        guard let titleData = title.data(using: .utf8),
            let photoData = photoImage.jpegData(compressionQuality: 1.0) else {
                completionBlock(false)
                return
        }
        DataService.editPhoto(path: "images/" + id,
                              title: titleData,
                              photoData: photoData) { (success) in
                                completionBlock(success)
        }
    }
    
    func deletePhoto(id: String, completionBlock: @escaping (Bool) -> Void) {
        DataService.deletePhoto(path: "images/" + id) { [weak self] response in
            let success = response.error == nil
            if success {
                self?.deleteCache(id: id)
            }
            completionBlock(success)
        }
    }
    
    func cache(image: UIImage, for photo: Photo) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let context = appDelegate.persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: context) else {
            return
        }
        let newPhoto = NSManagedObject(entity: entity, insertInto: context)
        newPhoto.setValue(photo.id, forKey: Constants.idKey)
        guard let imageData = image.jpegData(compressionQuality: 1.0) else {
            return
        }
        newPhoto.setValue(imageData, forKey: Constants.cacheKey)
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func cachedImage(for id: String) -> UIImage? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.entityName)
        request.predicate = NSPredicate(format: "id = %@", id)
        request.returnsObjectsAsFaults = false
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let context = appDelegate.persistentContainer.viewContext
        do {
            let result = try context.fetch(request)
            guard let photoEntity = result.first as? NSManagedObject else {
                return nil
            }
            guard let imageCache = photoEntity.value(forKey: Constants.cacheKey) as? Data,
                let image = UIImage(data: imageCache) else {
                return nil
            }
            return image
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func deleteCache(id: String) {
        photos.removeAll { $0.id == id }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.entityName)
        request.predicate = NSPredicate(format: "id = %@", id)
        request.returnsObjectsAsFaults = false
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let context = appDelegate.persistentContainer.viewContext
        do {
            let result = try context.fetch(request)
            for object in result {
                guard let object = object as? NSManagedObject else {
                    return
                }
                context.delete(object)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
