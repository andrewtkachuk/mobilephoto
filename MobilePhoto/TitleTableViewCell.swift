//
//  TitleTableViewCell.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 4/14/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit

protocol TitleTableViewCellDelegate: class {
    func didSetTitle(title: String)
}

class TitleTableViewCell: UITableViewCell {
    @IBOutlet private weak var titleTextField: UITextField!
    
    weak var delegate: TitleTableViewCellDelegate?
    var titleString: String? {
        didSet {
            titleTextField.text = titleString
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleTextField.delegate = self
    }

}

extension TitleTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.didSetTitle(title: textField.text ?? "")
    }
}
