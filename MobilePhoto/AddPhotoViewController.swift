//
//  AddPhotoViewController.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 4/14/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit

protocol AddPhotoControllerProtocol: class {
    func didPressCancel()
    func didSavePhoto()
}

private enum Constants {
    static let editTitle = "Edit Photo"
    static let titleCellIdentifier = "titleCell"
    static let imageCellIdentifier = "imageCell"
    static let titleCellHeight: CGFloat = 44.0
    static let imageCellHeight: CGFloat = 150.0
}

class AddPhotoViewController: UIViewController {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var waitingView: UIView!
    
    weak var delegate: AddPhotoControllerProtocol?
    private var photo: Photo!
    private var image: UIImage!
    private var photoTitle: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        if photo != nil {
            titleLabel.text = Constants.editTitle
        }
        waitingView.isHidden = true
    }

    @IBAction func cancelAdding(_ sender: UIButton) {
        delegate?.didPressCancel()
    }
    
    @IBAction func saveAdding(_ sender: UIButton) {
        if photo == nil {
            guard let image = image,
                let title = photoTitle else {
                    delegate?.didSavePhoto()
                    return
            }
            waitingView.isHidden = false
            PhotosManager.shared.uploadPhoto(title: title, photoImage: image) { [weak self] _ in
                self?.delegate?.didSavePhoto()
            }
        } else {
            if image == nil && photoTitle == nil {
                delegate?.didSavePhoto()
                return
            }
            let title = photoTitle ?? photo.title
            let imageToSend = image ?? PhotosManager.shared.cachedImage(for: photo.id)!
            waitingView.isHidden = false
            PhotosManager.shared.editPhoto(id: photo.id,
                                           title: title,
                                           photoImage: imageToSend) { [weak self] _ in
                                            PhotosManager.shared.deleteCache(id: self?.photo.id ?? "")
                                            self?.delegate?.didSavePhoto()
            }
        }
    }
    
    func configureForEditing(with photo: Photo) {
        self.photo = photo
    }
}

extension AddPhotoViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.titleCellIdentifier) as? TitleTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            if photo != nil {
                cell.titleString = photo.title
            }
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.imageCellIdentifier) as? ImageTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        if photo != nil, let cache = PhotosManager.shared.cachedImage(for: photo.id) {
            cell.photoImage = cache
        }
        if image != nil {
            cell.photoImage = image
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return Constants.titleCellHeight
        }
        return Constants.imageCellHeight
    }
}

extension AddPhotoViewController: ImageTableViewCellDelegate {
    func shouldPresentImagePicker(with source: UIView) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .popover
        present(imagePicker, animated: true, completion: nil)
    }
}

extension AddPhotoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        image = info[.originalImage] as? UIImage
        tableView.reloadData()
        dismiss(animated: true, completion: nil)
    }
}

extension AddPhotoViewController: TitleTableViewCellDelegate {
    func didSetTitle(title: String) {
        photoTitle = title
    }
}
