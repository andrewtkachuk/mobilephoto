//
//  PhotoCell.swift
//  MobilePhoto
//
//  Created by Andrew Tkachuk on 3/3/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit

protocol PhotoCellDelegate: class {
    func didPressDeletePhoto(id: String)
    func didPressEditPhoto(photo: Photo)
}

class PhotoCell: UITableViewCell {
    @IBOutlet private weak var photoView: UIImageView!
    @IBOutlet private weak var waitView: UIView!
    @IBOutlet private weak var photoTitle: UILabel!
    
    private var photo: Photo!
    weak var delegate: PhotoCellDelegate?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        waitView.isHidden = false
        photoTitle.text = ""
    }
    
    func configure(with photo: Photo) {
        if let cache = PhotosManager.shared.cachedImage(for: photo.id) {
            photoView.image = cache
            waitView.isHidden = true
        } else {
            DispatchQueue.global().async {
                PhotosManager.shared.loadPhoto(photo, completionBlock: { [weak self] image in
                    DispatchQueue.main.async {
                        self?.photoView.image = image
                        self?.waitView.isHidden = true
                    }
                })
            }
        }
        photoTitle.text = photo.title
        self.photo = photo
    }
    
    @IBAction func deletePhoto(_ sender: UIButton) {
        delegate?.didPressDeletePhoto(id: photo.id)
    }
    
    @IBAction func editPhoto(_ sender: UIButton) {
        delegate?.didPressEditPhoto(photo: photo)
    }
}
