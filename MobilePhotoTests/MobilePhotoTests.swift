//
//  MobilePhotoTests.swift
//  MobilePhotoTests
//
//  Created by Andrew Tkachuk on 3/2/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import MobilePhoto

class MobilePhotoTests: XCTestCase {

    override func setUp() {
        guard let stubPath = OHPathForFile("Photos.json", type(of: self)),
            let data = UIImage(named: "background")?.jpegData(compressionQuality: 1) else {
            return
        }
        stub(condition: isPath("https://cloudserviceapp.herokuapp.com/")) { _ -> OHHTTPStubsResponse in
            return fixture(filePath: stubPath, status: 200, headers: nil)
        }
        stub(condition: isPath("https://cloudserviceapp.herokuapp.com/images/5bc20e24fcbbc700045e7d90.jpg")) { _ -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(data: data, statusCode: 200, headers: nil)
        }
    }

    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
    }

    func testLoadingPhotos() {
        PhotosManager.shared.loadPhotos { result in
            XCTAssert(result)
            XCTAssertEqual(PhotosManager.shared.photos.count, 5)
        }
    }
    
    func testLoadingPhoto() {
        PhotosManager.shared.loadPhotos { _ in
            guard let photo = PhotosManager.shared.photos.first else {
                XCTAssert(false)
                return
            }
            PhotosManager.shared.loadPhoto(photo, completionBlock: { image in
                XCTAssertNotNil(image)
            })
        }
    }
}
