//
//  LocationTableViewCell.swift
//  LocationTest
//
//  Created by Andrew Tkachuk on 4/20/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit
import CoreLocation

class LocationTableViewCell: UITableViewCell {
    @IBOutlet private weak var latLabel: UILabel!
    @IBOutlet private weak var longLabel: UILabel!
    
    func configureLocation(_ location: CLLocation) {
        latLabel.text = location.coordinate.latitude.description
        longLabel.text = location.coordinate.longitude.description
    }

}
