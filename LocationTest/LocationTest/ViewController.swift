//
//  ViewController.swift
//  LocationTest
//
//  Created by Andrew Tkachuk on 4/20/19.
//  Copyright © 2019 Andrew Tkachuk. All rights reserved.
//

import UIKit
import CoreLocation

private let cellIdentifier = "locationCell"

class ViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    let locationManager = CLLocationManager()
    var locations: [CLLocation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locations.append(contentsOf: locations)
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? LocationTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configureLocation(locations[indexPath.row])
        return cell
    }
}
